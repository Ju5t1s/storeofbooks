<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('branch_id')->unsigned()->nullable();
            
            $table->string('contact_phone')->nullable();
            $table->date('reservation_date')->nullable();
          

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('branch_id');
            $table->dropColumn('number_of_persons');
            $table->dropColumn('contact_phone');
            $table->dropColumn('reservation_date');
            
        });
    }
}
