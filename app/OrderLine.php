<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{
    public $timestamps = false; 
    protected $fillable =[
    	'order_id',
    	'book_id',
    	'quantity',
    	'total'
   ];

   public function order()
   {
   	 return $this->belongsTo('App\Order');
   } 
   public function book()
   {
    return $this->belongsTo('App\Book');
   }
}
