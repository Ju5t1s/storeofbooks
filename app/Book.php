<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable =[
    	'title',
    	'photo',
    	'description',
      'year',
      'publisher',
    	'netto_price',
    	'price',
    	'quantity'
    ];
    public function getFormattedPriceAttribute($value)
    {
       return number_format($this->price, 2);
    }
    public function getFormattedNetoPriceAttribute($value)
    {
       return number_format($this->netto_price, 2);
    }
    public function order_lines()
   {
     return $this->hasMany('App\OrderLine');
   }

   public function getWithoutVat()
   {
       return \App\Price::getWithoutVat($this->FormattedPrice);
   }

   public function getVat()
   {
       return $this->FormattedPrice - $this->getWithoutVat();
   }
}

