<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
      protected $fillable = [
    	'name',
    	'photo',
    	'address',
    	'contact_phone',
        'email',
        'map',
        'hours'
    	

    ];

    public function orders()
    {
    	return $this->hasMany('App\Order');
    }

    public function __toString(){
        return $this->title ;
    }
}