<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class bookController extends Controller
{
    public function __construct() {
        $this->middleware('auth.admin')
             ->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('book.index', compact('books')); /**['books'=> $books] tas pats butu compact($books)*/
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('book.form', compact('book'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')){
            $file = $request->file('photo');
            $file->store('public');
            $photo= $file->hashName();
            }

       Book::create([
            'title' => $request->get('title'),
            'year' => $request->get('year'),
            'publisher' => $request->get('publisher'),
            'photo' => $photo,
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'netto_price' => $request->get('netto_price'),
            'quantity' => $request->get('quantity'),
          ]);


        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        return view('book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(book $book)
    {
        return view('book.form', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, book $book)
    {
           if ($request->hasFile('photo')){
            $file = $request->file('photo');
            $file->store('public');
            $book->photo = $file->hashName();
            }

           $book->title= $request->title;
            $book->year= $request->year;
           $book->description = $request->description;
           $book->price = $request->price;
           $book->netto_price = $request->netto_price;
           $book->quantity = $request->quantity;
           

           $book->update();

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::find($id)->delete();
        return redirect()->route('books.index');

    }
}
