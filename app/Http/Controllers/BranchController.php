<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;

class branchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
           $this->middleware('auth.admin')
                  ->except(['index', 'show']);
         }

    public function index()
    {
        $branches = Branch::all();
            return view('branches.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("branches.edit");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Branch::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'contact_phone' => $request->get('contact_phone'),
            'email' => $request->get('email'),
            'hours' => $request->get('hours'),
            'map' => $request->get('map')
            ]);

        return redirect()->route('branches.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(branch $branch)
    {
        $branch = Branch::find($branch);
        return view ('branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(branch $branch)
    {
        $branch = Branch::find($branch);
        return view('branches.edit', compact('branch'));    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, branch $branch)
    {
        Branch::find($branch)->update($request->all());
        return redirect()->route('branches.show', $branch);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(branch $branch)
    {
        Branch::find($branch)->delete();
        return redirect()->route('branches.index');
    }
}