<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;

class CheckoutController extends Controller
{
    public function info()
    {
        $branches = Branch::all()->pluck('name', 'id');

        return view('checkout.info', compact('branches'));
    }

    public function store(Request $request)
    {
$this->validate($request, [

         'name' => 'required|max:255',
          'branches' => 'required|exists:branches,id',
          
          'date' => 'nullable|date|date_format:Y-m-d',
          'phone' => 'required|numeric',
          

       ]);

        $reservationInfo = [
            'branch_id' => $request->branches,
            'name' => $request->name,
            'phone' => $request->phone,
            
            'date' => $request->date,
            
        ];



        session()->put('cart.reservation_info', $reservationInfo);

        return redirect()->route('checkout.confirm');
    }

    public function confirm()
    {
        $branch = Branch::find(session('cart.reservation_info.branch_id'));
        return view('checkout.confirm', compact('branch'));
    }
}
