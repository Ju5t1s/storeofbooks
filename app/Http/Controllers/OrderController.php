<?php

namespace App\Http\Controllers;

use App\Book;
use App\Order;
use App\OrderLine;
use App\Branch;
use Illuminate\Http\Request;
use App\Price;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.admin')
        ->except(['index', 'show', 'store' , 'addToCart', 'clearCart', 'deleteLine', 'checkout']);

        $this->middleware('auth')
        ->except(['addToCart', 'clearCart', 'deleteLine', 'checkout']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->isAdmin()) {
            $orders = Order::all();
        } else {
            // $orders = Order::where('user_id', \Auth::user()->id)->get();
            $orders = \Auth::user()->orders;
        }

        return view('order.index', ['orders'=> $orders]);
    }
//


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     if (session('cart.total')){
        return view('order.form');
    } else {
        return redirect()->route('books.index');
    }

}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create([
            'name' => session('cart.reservation_info.name'),
            'email' => \Auth::user()->email,
            'total' => session('cart.total'),
            'date' => \Carbon\Carbon::now(),
            'user_id' => \Auth::user()->id,
            'contact_phone' =>  session('cart.reservation_info.phone'),
            'reservation_date' => session('cart.reservation_info.date'),            
            'branch_id' => session('cart.reservation_info.branch_id')
            ]);

        foreach(session('cart.items') as $item) {
            OrderLine::create([
                'order_id' => $order->id,
                'book_id' => $item['id'],
                'quantity' => $item['quantity'],
                'total' => $item['total']
                ]);
        }
        $this-> clearCart();
        return redirect()->route('branches.index')->with('message', [
            'text' => 'Your order was placed successfully!',
            'type' => 'success'
            ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $branches = Branch::all()->pluck('name');
        $address = Branch::all()->pluck('address');
        return view('order.show', compact('order', 'branches', 'address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $books = Book::all()->pluck('title', 'id');
        $order = Order::find($id);
        $branches = Branch::all()->pluck('name', 'id');
        
        return view('order.form', compact('order', 'books', 'branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,order  $order)
    {
        
        


        $order->name= $request->name;
        $order->email = $request->email;
        
        $order->contact_phone = $request->phone;
        $order->date = \Carbon\Carbon::now();
        $order->reservation_date = $request->reservation_date;
          
          $order->update();
        return redirect()->route('orders.index');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */


    public function destroy(Order $order, Request $request)
    {
        $order->order_lines()->delete();
        $order->delete();

        if ($request->has('redirect_url')) {
            return redirect()->to($request->redirect_url);
        }

        return redirect()->route('orders.index');
    }

    public function addToCart(Request $request){
        // session(['cart.items'=> [] ]);
        $book_id = $request->id;

        $book = Book::find($book_id);
        $product = [
        'id' => $book->id,
        'photo' => $book->photo,
        'title' => $book->title,
        'quantity' => 1,
        'price' => $book->FormattedPrice,
        'total' =>$book->FormattedPrice,
        'without_vat' => $book->getWithoutVat(),
        'vat' => $book->getVat()
        ];
        $items = session('cart.items');
        $existing = false;
        $grand_total = 0;

        if ($items && count($items) > 0) {

            foreach($items as $index => $item) {
                if($item['id'] == $book_id) {
                    $items[$index]['quantity'] = $item['quantity'] + $product['quantity'];
                    $items[$index]['total'] = $items[$index]['quantity'] * $book->FormattedPrice;
                    $items[$index]['without_vat'] = $items[$index]['quantity'] * $book->getWithoutVat();
                    $items[$index]['vat'] = $items[$index]['quantity'] * $book->getVat();
                    $existing = true;
                    
                } 
            }
        }
        if(! $existing){
            session()->push('cart.items', $product);
            $grand_total += $product['total'];
        } else {
            session(['cart.items'=> $items]);
        }


        $this->calculateCartTotals();
        return session('cart');
    }

    public function clearCart() {
        session(['cart.items' => [],  'cart.total' =>0]);

    }   
    

    public function deleteLine(Request $request) {
        $id = $request->id;
        $items = session('cart.items');

        $total = session('cart.total');

        foreach ($items as $index => $item) {
            if ($item['id'] == $id) {
                $total -= $item['total'];
                unset($items[$index]);
                break;
            }
        }

        session([

            'cart.items' => $items,
            
            ]);
        $this->calculateCartTotals();

        return redirect()->route('cart.checkout');
    }
    private function calculateCartTotals() {
        $items = session('cart.items');
        $grand_total = 0;

        foreach($items as $item) {
            $grand_total = $grand_total + $item['total'];
        }


        $withoutVat = Price::getWithoutVat($grand_total);
        $vat = Price::getVat($grand_total);

        session([
            'cart.total_without_vat' => $withoutVat,
            'cart.total_vat' => $vat,
            'cart.total' => $grand_total
            ]);

    }


    public function checkout()
    {
        return view('order.checkout');
    }

    public function destroyLine($id) {
        $line= OrderLine::find($id);
        
        $line->order->total -= $line->total;
        $line->order->save();
        $line->delete();

        return redirect()->route('orders.edit', $line->order->id);
    }

    public function addToOrder($id, request $request){
        echo dd($request);

        $order =Order::find($id);
        $book=Book::find($request->book);
        $line = new \App\OrderLine();
        $line->order=$order;
        $line->quantity=$request->quantity;
        $line->book=$request->book;
        $line->total=$book->price * $request->quantity;


        echo dd($request);

        $line->save();

        return redirect()->route('orders.edit');

    }
}
