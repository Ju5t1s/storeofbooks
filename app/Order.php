<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
  protected $fillable =[
  'name',
  'email',
  'total',
  'date',
  'user_id',
  'reservation_date',
  
  'branch_id',
  
  'contact_phone'
  
  ];

  public function order_lines()
  {
   return $this->hasMany('App\OrderLine');
 }
 public function user()
 {
  return $this->belongsTo('App\User');
}

public function branch()
{
 return $this->belongsTo('App\Branch');
}

public function getReservationClass() : string
{
 /* šviesaforą rodome tik tada, kai žiūri administratorius */
 if (\Auth::check() && \Auth::user()->isAdmin()) {
   $date = Carbon::parse($this->reservation_date);

   if ($date->isToday()) {
     return 'danger';
   } else if ($date->isTomorrow()) {
     return 'info';
   } else if ($date->isPast()) {
     return 'success';
   }
 }

 return '';
}

public function getWithoutVat()
  {
   return \App\Price::getWithoutVat($this->total);
  }

public function getVat()
  {
   return $this->total - $this->getWithoutVat();
  }
  public function __toString(){
        return $this->title ;
    }
}



