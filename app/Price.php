<?php

namespace App;

class Price
{

    /**
     * grąžiną sumą be PVM
     */
    public static function getWithoutVat($value)
    {
        return number_format($value / 1.21, 2);
    }

    /**
     * grąžiną PVM
     */
    public static function getVat($value)
    {
        return number_format(
            $value - self::getWithoutVat($value),
            2
        );
    }

    public function centsToEuros($cents)
    {
        
    }
}