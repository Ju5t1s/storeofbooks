@extends('layouts.app')

@section('content')

<h2>Order list :</h2>

		<hr>
			<div class="row">
				@foreach ($orders as $order)
					<div class="col-md-6 col-md-offset-3"> 
						<ul>
							<li><h3>Name: {{ ucfirst($order->name) }} </h3> <p>E-mail: {{ $order->email }} </p> <p>Order total:{{ $order->total }} Eur.</p> <p>Date ordered: {{ $order->date }}</p>
							<p>Collect Date: {{ $order->reservation_date }}</p>
							<p>no VAT: {{ $order->getWithoutVAT() }} &euro; <br>
							Order price: {{ $order->total }} &euro;</p>
							<p><a href="{{route('orders.show', $order->id) }}" class="btn btn-primary" role="button">Show Details</a></p>
							
							</li>
							<hr>
						</ul>

							
									 
					</div>
		 		@endforeach	
			
			</div>
 		<hr>
 		@if(Auth::user() && Auth::user()->isAdmin())
 		<a href="{{route('orders.create') }}" class="btn btn-primary" role="button">Make new Order</a> 
 		@endif	
@endsection
