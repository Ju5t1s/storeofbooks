@extends('layouts.app')

	@section('content')
	<div class="container">
	<h2>Order details</h2>
	<hr>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<ul>
					<li>Ordered by: {{ $order->name }} </li>
					<li>Contact E-mail: {{ $order->email }} </li>
					<li>Date ordered: {{ $order->date }} </li>
					<li>Collect date: {{ $order->reservation_date }} </li>
					<li>Pickup branch: {{  $order->branch_id = $branches[0] }} </li>
					<li>Branch address : {{  $order->branch_id = $address[0] }} </li>
				</ul>
				<table class="table">
						<thead>
							<th>Name</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>Total</th>
							@if(Auth::user() && Auth::user()->isAdmin())
							<th>Delete</th>
							@endif
						</thead>
					@if(count($order->order_lines()) >0)
				       <tbody>
				        @foreach($order->order_lines as $item) 
				            <tr>         
				                <td>{{ $item->book->title }} <img src="{{ asset('/storage/' . $item->book->photo)}}" alt="{{ $item->book->title }}" class="img-rounded" style="width: 70px;"></td>
				                <td>{{ $item->quantity }} </td>
				                <td>{{ $item->book->price }} </td>
				                <td>{{ $item->total }} </td>
				                @if(Auth::user() && Auth::user()->isAdmin())
				                <td><a id="clear-line-cart" href="#" ><i class="fa fa-trash-o" aria-hidden="true"></i>X</a></td>
				                @endif
				            </tr>  
				        @endforeach
				        </tbody>
				        <tfoot>
			            	<tr>
			            		<td colspan="3" class="text-right">Total:</td>
			            		<td>{{ $order->total }}</td>
			            	</tr>
				        </tfoot>  
				    @endif
				</table>
   				
					@if(Auth::user() && Auth::user()->isAdmin())
	   		 <a href="{{route('orders.edit', $order->id) }}" class="btn btn-primary">Edit order</a>
			@endif 
			</div>
		</div>
	</div>
	@endsection

