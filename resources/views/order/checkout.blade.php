@extends('layouts.app')

@section('content')

<div class="table-responsive">
  <table class="table">
@if(!session('cart.items'))

  <h2>Your cart is empty</h2>
  <hr>
   <a href="{{ route('books.index') }} " class="btn btn-success">Browse book shelf</a>

@else

<h2>Your cart</h2>
<hr>


	<tbody>
		<thead>
			<th>Name</th>
      <th>Price for One</th>
			<th>Quantity</th>
			<th>Total</th>
      <th>VAT</th>
      <th>All w/ VAT</th>
			<th>Clear</th>
		</thead>
	@if(session('cart.items') && count(session('cart.items'))>0)
        @foreach(session('cart.items') as $item)
            <tr>
                <td>{{ $item['title']}} <img src="{{ asset('/storage/' . $item['photo'])}}" alt="{{ $item['title']}}" class="img-rounded" style="width: 70px;"> </td>
                <td>{{ $item['price']}} €</td>
                <td>{{ $item['quantity']}} </td>
                <td>{{ $item['total']}} €</td>
                <td>{{ $item['vat'] }}€</td>
                <td>{{ $item['without_vat'] }}€</td>

               <td><a class="clear-line-cart" href="{{ route('cart.delete_line', ['id' => $item['id']] ) }}"><i class="fa fa-trash-o" aria-hidden="true">X</i></a></td>
             </tr>

            @endforeach
            <tfoot>
            	<td></td>
            	<td></td>
            	<td>In total : </td>
              <td> {{session('cart.total') ?: '0'}} &euro;</td>
            	
              <td>{{ session('cart.total_vat') ?: '0' }} €</td>
              <td>{{ session('cart.total_without_vat') ?: '0' }} &euro;</td>
              
            </tfoot>
        

@endif
	</tbody>

  </table>
@if(Auth::check())
<a href="{{route('checkout.info') }}" class="btn btn-success">Continue</a>
@else
   <a href="{{ route('register') }}" class="btn btn-success">Continue and Register</a>
@endif
@if(session('cart.items') && count(session('cart.items'))==0)

 
  @endif

</div>
@endif

@endsection
