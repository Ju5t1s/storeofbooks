@extends('layouts.app')


@section('content')

<h2>Order edit:</h2>
<hr>

@if(isset($order))
{!! Form::model($order, [
	'route' => ['orders.update', $order->id], 'method' => 'put'
	]) !!}
	@else
	{!! Form::open(['route' => 'orders.store', 'method' => 'post']) !!}
	@endif

	<div class="from-group">
	{!! Form::label('name', 'Name') !!}    
		{!! Form::text('name', Auth::user() ? Auth::user()->name: null, ['class' => 'form-control', 'placeholder' => 'Name'])!!}
	</div>
	<div class="from-group">  
	{!! Form::label('email', 'Email') !!}  
		{!! Form::text('email', Auth::user() ? Auth::user()->email: null, ['class' => 'form-control', 'placeholder' => 'Email'])!!}
	</div>
	<div class="from-group">
	{!! Form::label('phone', 'Phone No.') !!}    
		{!! Form::text('phone', Auth::user() ? Auth::user()->phone: null, ['class' => 'form-control', 'placeholder' => 'Phone'])!!}
	</div>
	<div class="from-group"> 
	{!! Form::label('reservation_date', 'Date of collection') !!}
	  {!! Form::date('reservation_date', Auth::user() ? Auth::user()->date : null, ['class' => 'form-control', 'placeholder' => 'Date']) !!}
	  </div>
	
<br>
@if (Auth::check()) 

{!! Form::submit('Save' ,['class' => ' btn btn-warning']) !!}
{!! Form::close() !!}
@endif 	
<br>

@if(count($order->order_lines))
<table class="table">
	<tr>
		<th><i>Book</i></th>
		<th><i>Quantity</i></th>
		<th><i>Price</i></th>
		<th><i>Delete</i></th>
	</tr>

	@foreach($order->order_lines as $line)
		<tr>
			<td>{{ $line->book->title  }}</td>
			<td>{{ $line->book->quantity  }}</td>
			<td>{{ $line->book->price  }}</td>
			<td>{!! Form::open(['route' => ['orders.line_destroy', $line->id], 'method' => 'delete'])!!}
			{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
{!! Form::close() !!}</td>
		</tr>

@endforeach
</table>
@endif
	

@endsection