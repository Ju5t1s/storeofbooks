<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Arapey" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">

    <link href="{{ asset('css/projektas.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/css/bootstrap-formhelpers.min.css" />

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'cartAddRoute' => route('cart.add'),
            'cartClearRoute' =>route('cart.clear')
            // 'cartClearLineRoute' =>route('cart.clearline')
            ]) !!};
        </script>

    </head>

    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'Laravel') }}
                        </a>
                        <ul class="nav navbar-nav main-dropdown">
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Main<span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                @if (Auth::check())
                                <li><a href="{{route('books.index') }}">Menu</a></li>
                                <li><a href="{{route('orders.index') }}">Your Orders</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Total: <span id="cart-total">{{session('cart.total') ?: '0'}}</span> &euro; <span class="caret"></span>
                            </a>
                            <ul id="cart-items" class="dropdown-menu" role="menu">
                                @if(session('cart.items') && count(session('cart.items'))>0)

                                @foreach(session('cart.items') as $item)
                                <li>
                                    <a href="{{ route('cart.checkout')}}">{{ $item['title']}} x {{ $item['quantity'] }} pcs., Total:<strong>{{ $item['total']}} &euro; </strong></a>
                                </li>
                                @endforeach
                                @endif
                            </ul>

                        </li>
                        <li>
                            <a id="checkout" href="{{ route('cart.checkout')}}" id="clear-cart">My cart</a>
                        </li>
                        <li>
                            <a href="#" id="clear-cart">Clear cart</a>
                            <li>
                                <a href="{{ route('branches.index') }}">Branches</a>
                            </li>
                        </li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Total: <span id="cart-total">{{session('cart.total') ?: '0'}}</span> &euro; <span class="caret"></span>
                            </a>
                            <ul id="cart-items" class="dropdown-menu" role="menu">
                                @if(session('cart.items') && count(session('cart.items'))>0)

                                @foreach(session('cart.items') as $item)
                                <li>
                                    <a href="{{ route('cart.checkout')}}">{{ $item['title']}} x {{ $item['quantity'] }} pcs., Total: <strong>{{ $item['total']}} &euro; </strong></a>
                                </li>
                                @endforeach
                                @endif
                            </ul>

                        </li>
                        <li>
                            <a id="checkout" href="{{ route('cart.checkout')}}" id="clear-cart">My cart</a>
                        </li>
                        <li>
                            <a href="#" id="clear-cart">Clear cart</a>
                        </li>
                        <li>
                                        <a href="{{ route('branches.index') }}">Branches</a>
                                    </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ ucfirst(Auth::user()->name) }} 
                                @if(Auth::user() && Auth::user()->isAdmin())
                            - {{ Auth::user()->type }}<span class="caret">
                                @endif
                            </span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('profile') }}">Profile</a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @if(Auth::user() && Auth::user()->isAdmin())
                            <li>
                                <a href="{{ route('users.index') }}">Users</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>
@if(session('message'))
<div class="alert alert-{{ session('message')['type']}}">
    {{session('message')['text']}}</div>
    @endif
    @yield('content')
    <footer>
        <p class="pull-right">© 2017 {{ config('app.name', 'Laravel') }}, Inc.</p>
        <p>· <a href="#">Privacy</a> · <a href="#">Terms</a></p>
    </footer>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>

    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
