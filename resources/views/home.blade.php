@extends('layouts.app')

@section('content')

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="2500">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="http://bgfons.com/upload/books_texture3024.jpg" alt="Books">
      <div class="carousel-caption">
        
      </div>
    </div>
    <div class="item">
      <img src="https://assets.entrepreneur.com/content/16x9/822/20150729211514-man-reading-book-hipster.jpeg" alt="Books">
      <div class="carousel-caption">
        
      </div>
    </div>
    <div class="item">
      <img src="http://www.startupremarkable.com/wp-content/uploads/2015/02/a-book-a-week-image.jpg" alt="Books">
      <div class="carousel-caption">
        
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="shop-now btn btn-warning">
    <a href="{{ route('books.index')}}">Shop now</a>
</div>
@endsection
