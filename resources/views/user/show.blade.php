@extends('layouts.app')

@section('content')
<div class="container">
	<h1>User information</h1>

    <div class="row">
        <div class="col-md-6">
            <dl class="dl-horizontal">
              <dt>Name</dt>
              <dd>{{ $user->name }}</dd>
            </dl>
        </div>
        <div class="col-md-6">
            <dl class="dl-horizontal">
                <dt>Address</dt>
                <dd>{{ $user->address }}</dd>
                <dt>City</dt>
                <dd>{{ $user->city }}</dd>
                <dt>Country</dt>
                <dd>{{ $user->getCountryName() }}</dd>
            </dl>
        </div>
    </div>

    <div class="row">
        <h2>Orders</h2>
        <hr />
        <table id="table-with-sorting" class="table table-bordered">
            <thead>
                <tr>
                    <th>Order Id</th>
                    <th>Total price</th>
                    <th>Dishes count</th>
                    <th>Contact Name</th>
                    <th>Phone</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user->getOrdersByPrice() as $order)
                    <tr class="{{ $order->getReservationClass() }}">
                        <td>#{{ $order->id }}</td>
                        <td>{{ $order->total }}€</td>
                        <td>{{ $order->order_lines->sum('quantity') }}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>
                            {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete'])!!}
                                <a href="{{ route('orders.show', $order) }}" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span> Peržiūra</a>
								{!! Form::hidden('redirect_url', route('users.show', $user)) !!}
                				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
                			{!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
