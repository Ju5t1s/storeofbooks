@extends('layouts.app')


	@section('content')
		
		<h2>Edit your book</h2>

		@if(isset($book))
			{!! Form::model($book, [
			'route' => ['books.update', $book->id],
			'method' => 'put',
			'files' => true
			]) !!}
		@else
			{!! Form::open(['route' => 'books.store', 'method' => 'post', 'files' => true]) !!}
		@endif
			
			<div class="from-group">    
			{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			
			<div class="from-group">  
			{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
			</div>
			
			
			
						
			<div class="from-group">  
			{!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
			</div>

			<div class="from-group">  
			{!! Form::text('publisher', null, ['class' => 'form-control', 'placeholder' => 'Publisher']) !!}
			</div>

			<div class="from-group">  
			{!! Form::text('year', null, ['class' => 'form-control', 'placeholder' => 'Year']) !!}
			</div>

			<div class="from-group">  
			{!! Form::text('netto_price', null, ['class' => 'form-control', 'placeholder' => 'Netto Price']) !!}
			</div>
			
			<div class="from-group">  
			{!! Form::number('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
			</div>


@if(isset($book) && $book->photo)
	<img class="book-form-photo" src="{{ asset('/storage/' . $book->photo)}}">
	@endif
			<div class="from-group">  
			{!! Form::file('photo', null, ['class' => 'form-control', 'placeholder' => 'IMAGE']) !!}

			</div>
	@if (Auth::check()) 

			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
	
		@endif 	

		{!! Form::close() !!}



		@if(isset($book))
			{!! Form::open(['route' => ['books.destroy', $book->id], 'method' => 'delete'])!!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			{!! Form::close() !!}
		@endif

@endsection