@extends('layouts.app')

	@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<h2><i>{{ $book->title }}</i></h2>
				<p><i>by: </i><a href="">{{ $book->publisher }}</a></p>
				<hr>
				<div>
					<img src="{{ asset('/storage/' . $book->photo)}}">
				</div>
				<hr>
				<p>Story:</p>
				<p> {{ $book->description }}</p>
				<ul>
					<li>Price: <i>{{ $book->Formattedprice }} &euro;</i></li>
					<li>Books Left: <i> {{ $book->quantity }}pcs.</i></li>
					@if(Auth::user() && Auth::user()->isAdmin())
					<li>
					Netto :
					<i>{{ $book->FormattedNetoPrice }} 
					&euro;</i></li>
					@endif
					<li>Year published: <i>{{ $book->year }}</i></li>
				</ul>

				<a href="#" class="btn btn-warning add-to-cart"  data-id="{{ $book->id }}" role="button">Add to cart</a> 
			@if(Auth::user() && Auth::user()->isAdmin())
	   		 <a href="{{route('books.edit', $book->id) }}" class="btn btn-primary">EDIT</a>
			@endif 

		</div>

	</div>

	</div>

		
	@endsection