@extends('layouts.app')

@section('content')

<h2>We Love Books...</h2>

@if(Auth::user() && Auth::user()->isAdmin())
<a href="{{route('books.create') }}" class="btn btn-warning" role="button">Add New Book to Menu</a> 
@endif		
<hr>
@foreach ($books->chunk(4)  as $chunk) 
<div class="row">
	@foreach ($chunk as $book)
	<div class="col-md-3"> 
		<div class="thumbnail">
			<img class="img-responsive book-cover" src="{{ asset('/storage/' . $book->photo)}}" >

			<div class="caption">
				<h3><i> {{ $book->title }}</i> </h3>
				<p><i> by:</i> <a href="#">{{ $book->publisher }}</a> </p>
				<p> {{ mb_strimwidth($book->description, 0 , 80, "...")  }} </p>
				<p>
					<strong>Year published:</strong>
					<i>{{ $book->year }} 
					</i></p>
					<p>
					<strong>Price:</strong>
					<i>{{ $book->FormattedPrice }} 
					EUR</i>
					@if(Auth::user() && Auth::user()->isAdmin())
					<strong>Netto :</strong>
					<i>{{ $book->FormattedNetoPrice }}</i>
					<strong>EUR</strong>
					@endif</p>
					<br>
					<p>
					<strong>Quantity:</strong>
					<i>{{ $book->quantity }} 
					pcs.</i>
				</p>
				<p>
					<a href="{{route('books.show', $book->id) }}" class="btn btn-primary" role="button">Show more</a>
					<a href="#" class="btn btn-warning add-to-cart"  data-id="{{ $book->id }}" role="button">Add to cart</a>  
				</p>
			</div>			
		</div>
	</div>
	<br>
	@endforeach	
</div>
@endforeach	
@endsection
