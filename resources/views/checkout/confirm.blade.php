@extends('layouts.app')

@section('content')
    <h1>Confirmation</h1>

    <ul>
        <li>Name: {{ ucfirst(session('cart.reservation_info.name')) }}</li>
        <li>Phone: {{ session('cart.reservation_info.phone') }}</li>
        <li>Email: {{ \Auth::user()->email }}</li>
        <li>Order date: {{ \Carbon\Carbon::now() }}</li>
        <li>Collection date: {{ session('cart.reservation_info.date') }}</li>
        <li>Pick up at : {{ ucfirst($branch->name) }}, {{ ucfirst($branch->address) }}</li>
        <li>Total price: {{ session('cart.total') }} &euro;</li>
       
    </ul>

    {!! Form::open(['route' => ['orders.store'], 'method' => 'post'])!!}
        {!! Form::submit('Confirm' , ['class' => 'btn btn-success'])!!}
    {!! Form::close() !!}
@endsection
