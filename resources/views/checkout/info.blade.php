@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if(!session('cart.items'))
<h2>Empty cart</h2>

<div class="alert alert-{{ session('message')['type']}}">
    You can't order empty cart</div>
    <a href="{{ route('books.index') }} " class="btn btn-success">Browse book shelf</a>
    @else

    <h2>Enter your contact information</h2>
    <hr>

    {!! Form::open(['route' => 'checkout.store', 'method' => 'post']) !!}

    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', Auth::user() ? Auth::user()->name : null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}

    {!! Form::label('branches', 'Choose your branch : ') !!}
    {!! Form::select('branches', $branches, null, ['class' => 'form-control', 'placeholder' => 'Select pickup place']) !!}

    {!! Form::label('phone', 'Phone NO.') !!}
    {!! Form::text('phone', Auth::user() ? Auth::user()->phone : null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}    

    {!! Form::label('date', 'Date of Reservation') !!}
    {!! Form::date('date', Auth::user() ? Auth::user()->date : null, ['class' => 'form-control', 'placeholder' => 'Date']) !!}

    <hr>

    {!! Form::submit('Order' ,['class' => 'btn btn-warning']) !!}

    {!! Form::close() !!}

@endif

@endsection
