@extends('layouts.app')

@section('content')
@foreach ($branches->chunk(4) as $chunk)
<div class="container">

	<h1 style="text-align: center;"> Book a branch: </h1>
	@if(Auth::user() && Auth::user()->isAdmin())
<a href="{{ route ('branches.create') }}" class="btn btn-warning">Add new branch</a>
@endif
<hr>
	<div class='row'>

		@foreach ($chunk as $branch)
		<div class="col-md-6">
			<div class="panel">
				<div class="thumbnail">

					
					<div class="caption">
						<h3>{{ ucfirst($branch->name) }}</h3>
						
						<p>
							Address: {{ $branch->address }}
						 </p>
						<p>
							Tel NO. : <a href="#">{{ $branch->contact_phone }}</a>
						</p>
						<p>
							E-mail : <a href="#">{{ $branch->email }}</a>
							
						</p>
						<a href="{{ route('branches.show', $branch->id) }}" class="btn btn-warning" role="button"> Look</a> 
						 @if(Auth::user() && Auth::user()->isAdmin())
						<a href="{{ route('branches.edit', $branch->id, 'branches.destroy') }}" class="btn btn-warning"> Edit</a>
						</div>
						 <br>
{!! Form::open (['route' => ['branches.destroy', $branch->id], 'method' => 'DELETE']) !!}
		{!! Form::submit('Delete', ['class' => 'btn btn-danger'])  !!}
	{{ Form::close() }}	
			@endif
		
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	




@endforeach

@endsection