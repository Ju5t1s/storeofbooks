@extends('layouts.app')
@section('content')




    
    <div class="container">
    <div class='row'>
    	
        <div class="col-md-6">
        	<div class="panel">
           <div class="thumbnail">
            
           
            <div class="caption">
            <h3>{{ ucfirst($branch->name) }}</h3>
            
            <p>
              Address: {{ $branch->address }} </p>
              <p>
              Tel NO. : <a href="#">{{ $branch->contact_phone }}</a>
            </p>
            <p>
              E-mail : <a href="#">{{ $branch->email }}</a>
            </p>

             @if(Auth::user() && Auth::user()->isAdmin())
              <a style="color: white;" href="{{ route('branches.edit', $branch->id) }}" class="btn btn-primary"> Redaguoti</a>
              @endif
           		</div>
              
            </div>
          </div>
        </div>
        <div class="col-md-6">
        <div class="panel">
         <div class="thumbnail">
          <p>&nbsp;</p>
<table style="height: 190px; margin-left: auto; margin-right: auto;" width="289">
<tbody>
<tr>
<td style="width: 89px;">Day</td>
<td style="width: 89px; text-align: center;">Working hours</td>
</tr>
<tr>
<td style="width: 89px;">Monday</td>
<td style="width: 89px; text-align: center;">8:00 - 20:00</td>
</tr>
<tr>
<td style="width: 89px;">Tuesday</td>
<td style="width: 89px; text-align: center;">8:00 - 20:00</td>
</tr>
<tr>
<td style="width: 89px;">Wednesday</td>
<td style="width: 89px; text-align: center;">8:00 - 20:00</td>
</tr>
<tr>
<td style="width: 89px;">Thursday</td>
<td style="width: 89px; text-align: center;">8:00 - 20:00</td>
</tr>
<tr>
<td style="width: 89px;">Friday</td>
<td style="width: 89px; text-align: center;">8:00 - 20:00</td>
</tr>
<tr>
<td style="width: 89px;">Saturday</td>
<td style="width: 89px; text-align: center;">10:00 - 18:00</td>
</tr>
<tr>
<td style="width: 89px;">Sunday</td>
<td style="width: 89px; text-align: center;">Closed</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>



<div class="ephox-sloth-bin ephox-sloth-bin_39802931811491483696231" style="position: fixed; top: 0px; width: 100px; height: 100px; overflow: hidden; opacity: 0; left: -100000px;" contenteditable="true"><!-- x-tinymce/html -->DayWorking hours</div>
        
        <div >
          <h3>Where to find</h3>
                <iframe src="{{ $branch->map }}"  height="400" frameborder="0" style="border:0; width:700px; text-align: center;" allowfullscreen></iframe></iframe>

        </div>
        </div>
        </div>
       
     
       
       @endsection