@extends ('layouts.app')
@section('content')


@if(isset($branch))
{!! Form::model($branch , ['route' => ['branches.update', $branch->id], 'method' => 'put'  ]) !!}
@else
{!! Form::open(['route' => ['branches.store'], 'method' => 'POST'  ]) !!}
@endif
<div class="container">

<form>
	
<ul>
	<div class="panel panel-primary branch-create-form">
	<li><div class="form-group">
                
		<p>Title</p>{!! form::text ('name', null,['class'=>"form-control",'placeholder'=>'Branch Title'])!!}
	</div></li>
	
	<li>
	<div class="form-group">
                
		<p>Address</p>{!! form::text  ('address', null,['class'=>"form-control",'placeholder'=>'Address'])!!}
	</div></li>
	<li>
	<div class="form-group">
                
		<p>Email</p>{!! form::text  ('email', null,['class'=>"form-control",'placeholder'=>'Email'])!!}
	</div></li>
	<div class="form-group">
                <label for="map">Branch map:</label>
                {!!Form::text('map',null,['class'=>"form-control",'placeholder'=>'Map URL'])!!}
              </div>
	<li>
	<div class="form-group">
                
		<p>Working Hours - Table </p>{!! form::text  ('hours', null,['class'=>"form-control",'placeholder'=>'Working hours'])!!}
	</div></li>
	<li>
	<div class="form-group">
                
		<p>Phone number</p>{!! form::number  ('contact_phone', null,['class'=>"form-control",'placeholder'=>'Phone number'])!!}
	</div></li>
	
	{{Form::submit('Save!' , ['class' => 'btn btn-warning'])}}
	{{ Form::close() }}
<br>
	@if(isset($branch))

	{!! Form::open (['route' => ['branches.destroy', $branch->id], 'method' => 'DELETE']) !!}
		{!! Form::submit('Delete' , ['class' => 'btn btn-danger']) !!}
	{{ Form::close() }}
	@endif
	</div>
</ul>
</form>
</div>


@endsection