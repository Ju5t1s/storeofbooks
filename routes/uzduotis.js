1. registracijos formą, kuri susideda iš laukų: 'YRAAAAAA'
        --- Name
        --- Surname
        --- Date of birth
        --- Phone number
        --- Email
        --- Password
        --- Password repeat
        --- Address
        --- City
        --- Zip code
        --- Country (dropdown - optional)

[9:01]  
1.1 registracijos validaciją:  'YRAAAAAA'
        --- Name required
        --- Surname required
        --- Date of birth required, format YYYY-MM-DD
        --- Phone number required
        --- Email required, is valid email address
        --- Password required
        --- Password repeat must match with password
        --- Address required
        --- City required
        --- Zip code required, only numbers
        --- Country required (edited)

[9:02]  
1.2 po sėkmingos registracijos:  'YRAAAAAAAA'
        --- Saves user info to database
        --- Logins user to system
        --- Redirects user to Dishes menu page

[9:02]  
2. prisijungimas forma:    'YRAAAAAAAA'
        --- Logs user into system
        --- Redirects to Orders menu page

[9:03]  
3. vartotojo profilio redagavimą:     'YRAAAAAAAA'
   -- Same as in Registration with pre-filed user data

[9:04]  
4. logout funkcionalumas:     'YRAAAAAAAA'
    -- Logs out current user
    -- Redirects to home page

[9:05]      'YRAAAAAAAA'
5. patiekalo pridėjimas į krepšelį iš sąrašo (Dish Menu -> Add to cart button) (edited)

[9:05]    'YRAAAAAAAA'
6. Administratorius turi galėti kurti, redaguoti, trinti patiekalus

[9:10]  'yraaaaaa '
7. krepšelio atvaizdavimas
    -- patiekalų atvaizdavimas
        --- Name
        --- Price
        --- Picture
        -- Totals
        --- Without tax
        --- Tax
        --- With tax
    -- Checkout button
        --- Redirects user to registration if not logged in -----
        --- Saves order info to Sessions
        --- Redirects user to Confirm page

[9:11]   'yraaaaaa '
8. po sėkmingo užsakymo pateikimo
   --- Saves reservation info into database
    --- Redirects user to Contacts page

[9:11]  'yraaaaaa '
9. kontaktų puslapis
    -- Content
        --- Show google maps
        --- Show basic info about the restaurant (name, location, working hours)

[9:12]  'yraaaaaa '
10. kontaktų puslapio redagavimas (tik administratoriaus rolę turinčiam vartotojui). Papildomai - pridėkite WYSIWYG redaktorių

[9:15]  'yraaaaaa '
11. užsakymų sąrašas
    -- Prisijungusiam paprastam vartotojui
        --- Order ID
        --- Customer name, surname
        --- Total amount
        --- Tax amount
        --- Order creation date
          ''yraaaaaa ''
   -- Administratoriui
        --- All users orders
        --- Totals of all items in list

Apribojimai: užsakymas gali turėti tik 1 staliuko rezervaciją

[9:17]  'yraaaaaa '
12. pagrindinis template’as pas visus turi turėti:
               -- Restaurant logo 'tik ne logo'
               -- Restaurant title
               -- Current page title
               -- Total items in cart + link to cart
               -- Dishes list

[9:19]  'yraaaaaa ' 
13. vartotojų sąrašas (tik administratoriui)
           --- User ID
           --- Name surname
           --- Email
           --- Date of birth
           --- Phone number
       -- Actions 
           --- Delete user (deactivate optional)
           --- No delete button for current admin user
           --- show user (show orders in `show.blade.php`)

[9:20]  'yraaaaaa '
14. kuriant užsakymą, automatiškai užpildome - name, phone laukus (juos galima redaguoti)

[9:20]  
*Tolimesni darbai*

[9:22]  'bus'
15. administratoriui užsakymus išrūšiuojame nuo naujausio iki seniausio
  // $orders = order::orderBy('date', 'desc')->get(); -- arba query 
[9:24]  
16. užsakymo redagavimas - leidžiame redaguoti kontaktinę informaciją (name, email, number_of_persons, reservation_date & reservation_time)

[9:24]  'yraaaaaa '
16.1 pritaikome validacijos taisykles:
   --- Name required
   --- Number of persons at least one
   --- Date required, format: YYYY-MM-DD
   --- Time required, format: HH:mm
   --- Phone required

[9:25]  
16.2 leidžiame iš užsakymo priimti bei pridėti patiekalą

[9:27] 
17. jeigu vartotojas jungiasi ir neturi užsakymų, nukreipiame į titulinį puslapį. Jungimosi metu patikriname



EGZASS

