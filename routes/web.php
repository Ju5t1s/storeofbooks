<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/	
Route::get('/', 'HomeController@index');



Auth::routes();

Route::resource('books', 'BookController');
Route::resource('orders', 'OrderController');
Route::resource('branches', 'BranchController');
Route::resource('users',  'UserController');

//cart route
Route::post('cart', 'OrderController@addToCart')->name('cart.add');
Route::delete('cart', 'OrderController@clearCart')->name('cart.clear');
Route::get('cart', 'OrderController@checkout')->name('cart.checkout');
Route::get('cart/delete/{id}', 'OrderController@deleteLine')->name('cart.delete_line');
Route::get('cart/reservation', 'CheckoutController@info')->name('checkout.info');
Route::post('cart/reservation', 'CheckoutController@store')->name('checkout.store');
Route::get('cart/confirm', 'CheckoutController@confirm')->name('checkout.confirm');

// user routes
Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::put('/profile', 'UserController@update')->name('profile.update')->middleware('auth');


//
Route::delete('orders/line/{id}/destroy', 'OrderController@destroyLine')->name('orders.line_destroy');


